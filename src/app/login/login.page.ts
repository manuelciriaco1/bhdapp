import { Component, OnInit } from '@angular/core';
import { MainService } from '../services/main.service'
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  username:string = "";
  password:string = "";
  responseLogin:string = "";
  version:string = "0";
  constructor(private main:MainService, private translate: TranslateService) { 

  }

  ngOnInit() {
    this.translate.use('es')
  }

  async login() {
    if(this.username == '' || this.password == ''){
      this.main.presentAlert("Favor completar los campos requeridos");
      return;
    }
    this.main.login(this.username, this.password)
  }

}
