import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  private creditCards:any[]=[]
  private debitCards:any[]=[]

  constructor(private http:HttpClient,
    private main:MainService) {
    }

  ionViewWillEnter(){
    if(!this.main.getUserSession('access_token'))
      this.main.logout()
      
    this.getDebitCards()
    this.getCreditCards()
  }

  async getDebitCards(){
    const loadi = this.main.presentLoading('', 0);
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'authorization': 'Bearer '+localStorage.getItem('access_token')
        })
      };
      this.http.get(this.main.urlBackend + 'products/accounts', httpOptions).subscribe({
        next: async (data: any) => {
          //saving user data  
          this.debitCards = data;
          console.log(this.debitCards);
          (await loadi).dismiss()
        },
        error: async error => {
          (await loadi).dismiss();
          this.main.presentAlert("Error al obtener datos del servidor");
        }
      })
    } catch (error) {
      this.main.presentAlert(error.error); 
    }
    
  }

  async getCreditCards(){
    const loadi = this.main.presentLoading('', 0);
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'authorization': 'Bearer '+localStorage.getItem('access_token')
        })
      };
      this.http.get(this.main.urlBackend + 'products/credit_cards', httpOptions).subscribe({
        next: async (data: any) => {
          //saving user data  
          this.creditCards = data;
          console.log(this.creditCards);
          (await loadi).dismiss()
        },
        error: async error => {
          (await loadi).dismiss();
          this.main.presentAlert("Error al obtener datos del servidor");
        }
      })
    } catch (error) {
      this.main.presentAlert(error.error); 
    }
    
  }

  _lastfour(number:string){
    return number.slice(9,13)
  }

}
