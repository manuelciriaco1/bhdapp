import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  urlBackend = "https://bhdleonfrontend-test.herokuapp.com/"
  tabSelected = "tab1"

  constructor(private toast: ToastController,
    private loadingController: LoadingController,
    private http: HttpClient,
    private navCtrl: NavController,
    private router: Router
  ) {}

  login(username: string, password: string) {
    const loadi = this.presentLoading('', 0);
    try {
      this.http.post(this.urlBackend + 'sign_in/', {
        userId: username,
        password: password
      }).subscribe({
        next: async (data: any) => {
          //saving token 
          this.setUserSession('access_token', data.access_token);
          this.setUserSession('refresh_token', data.refresh_token);
          //retriving user data 
          this.getUserData(loadi);
        },
        error: async error => {
          (await loadi).dismiss();
          this.presentAlert("Usuario y/o contraseña incorrectos");
        }
      })
    } catch (error) {
      this.presentAlert(error.error);
    }
  }

  async getUserData(loadi: any) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'authorization': 'Bearer ' + localStorage.getItem('access_token')
        })
      };
      this.http.get(this.urlBackend + 'user_data/', httpOptions).subscribe({
        next: async (data: any) => {
          //saving user data  
          this.setUserSession('name', data.name);
          this.setUserSession('lastName', data.lastName);
          this.setUserSession('photo', data.photo);
          (await loadi).dismiss()
          this.home();
        },
        error: async error => {
          (await loadi).dismiss();
          this.presentAlert("Error al obtener datos del servidor");
        }
      })
    } catch (error) {
      this.presentAlert(error.error);
    }

  }

  async presentAlert(m) {
    const alert = await this.toast.create({
      message: m,
      duration: 2500,
      position: 'top',
      cssClass: "toast-scheme",
    });
    alert.present();
  }

  async presentLoading(m: string, t: number) {
    const loading = this.loadingController.create({
      message: m,
      duration: (typeof t == 'undefined' ? 0 : t),
    });
    (await loading).present();
    return loading
  }

  home() {
    this.navCtrl.navigateRoot(['tabs']);
  }

  setUserSession(key: string, value: string) {
    localStorage.setItem(key, value)
  }

  getUserSession(key: string) {
    return localStorage.getItem(key)
  }

  logout() {
    localStorage.clear()
    this.navCtrl.navigateRoot(['']);
  }

}
