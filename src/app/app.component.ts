import { Component } from '@angular/core';
import { MainService } from './services/main.service';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  private lastName:string=""
  private name:string=""
  private photo:string=""

  constructor(private main:MainService,
    public menuCtrl: MenuController,
    private route:Router) {
    this.lastName  = this.main.getUserSession('lastName')
    this.name       = this.main.getUserSession('name')
    this.photo      = this.main.getUserSession('photo')
  }

  logout(){
    this.menuCtrl.close()
    this.main.logout()
  }

  tab1(){
    
  }

  tab2(){
    
  }

  tab3(){
    
  }

  tab4(){
    
  }
}
